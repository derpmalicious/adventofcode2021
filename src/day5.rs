use std::fs;

#[derive(Debug)]
struct V {
    x: usize,
    y: usize
}

#[derive(Debug)]
struct Line {
    src: V,
    dst: V
}

fn print_board(board: &Vec<Vec<i32>>){
    let x_len = board.len();
    let y_len = board.first().unwrap().len();

    for y in 0..y_len {
        for x in 0..x_len {
            if board[x][y] == 0 {
                print!(".");
            }else{
                print!("{}", board[x][y]);
            }
        }
        println!();
    }
}

pub fn pt1() {
    let input = fs::read_to_string("inputs/day5p1.txt").unwrap();

    let mut lines = Vec::new();
    let mut max_x = 0;
    let mut max_y = 0;

    let mut board = Vec::new();

    // Construct lines from input
    for line in input.lines() {
        let (src, dst) = line.split_once(" -> ").unwrap();
        let (src_x, src_y) = src.split_once(",").unwrap();
        let (dst_x, dst_y) = dst.split_once(",").unwrap();

        let src = V {x: src_x.parse().unwrap(), y: src_y.parse().unwrap() };
        let dst = V {x: dst_x.parse().unwrap(), y: dst_y.parse().unwrap() };

        if src.x > max_x { max_x = src.x; }
        if src.y > max_y { max_y = src.y; }

        if dst.x > max_x { max_x = dst.x; }
        if dst.y > max_y { max_y = dst.y; }

        let line = Line{ src, dst };

        lines.push(line);
    }

    max_x += 1;
    max_y += 1;

    // Construct board
    let mut board_y_preset = Vec::new();

    for i in 0..max_y {
        board_y_preset.push(0);
    }

    for i in 0..max_x {
        board.push(board_y_preset.clone());
    }

    // Draw lines
    for line in lines {
        // Horizontal
        if line.src.y == line.dst.y {
            let mut a;
            let mut b;

            // Rust ranges don't operate backwards apparently?
            if line.src.x > line.dst.x {
                a = line.dst.x;
                b = line.src.x;
            }else{
                a = line.src.x;
                b = line.dst.x;
            }

            println!("Drawing horizontal line {:?}", line);
            for x in a ..= b {
                println!("Drawing to {}, {}", x, line.src.y);
                board[x][line.src.y] += 1;
            }
        }else if line.src.x == line.dst.x { // Vertical
            let mut a;
            let mut b;

            // Rust ranges don't operate backwards apparently?
            if line.src.y > line.dst.y {
                a = line.dst.y;
                b = line.src.y;
            }else{
                a = line.src.y;
                b = line.dst.y;
            }

            println!("Drawing vertical line {:?}", line);
            for y in a ..= b {
                println!("Drawing to {}, {}", line.src.x, y);
                board[line.src.x][y] += 1;
            }
        }else{
            println!("Line {:?} neither horizontal or vertical", line);
        }
    }

    print_board(&board);

    let mut overlaps = 0;

    for a in board {
        for b in a {
            if b > 1 {
                overlaps += 1;
            }
        }
    }

    println!("{}", overlaps);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day5p1.txt").unwrap();

    let mut lines = Vec::new();
    let mut max_x = 0;
    let mut max_y = 0;

    let mut board = Vec::new();

    // Construct lines from input
    for line in input.lines() {
        let (src, dst) = line.split_once(" -> ").unwrap();
        let (src_x, src_y) = src.split_once(",").unwrap();
        let (dst_x, dst_y) = dst.split_once(",").unwrap();

        let src = V {x: src_x.parse().unwrap(), y: src_y.parse().unwrap() };
        let dst = V {x: dst_x.parse().unwrap(), y: dst_y.parse().unwrap() };

        if src.x > max_x { max_x = src.x; }
        if src.y > max_y { max_y = src.y; }

        if dst.x > max_x { max_x = dst.x; }
        if dst.y > max_y { max_y = dst.y; }

        let line = Line{ src, dst };

        lines.push(line);
    }

    max_x += 1;
    max_y += 1;

    // Construct board
    let mut board_y_preset = Vec::new();

    for i in 0..max_y {
        board_y_preset.push(0);
    }

    for i in 0..max_x {
        board.push(board_y_preset.clone());
    }

    // Draw lines
    for line in lines {
        // Horizontal
        if line.src.y == line.dst.y {
            let mut a;
            let mut b;

            // Rust ranges don't operate backwards apparently?
            if line.src.x > line.dst.x {
                a = line.dst.x;
                b = line.src.x;
            }else{
                a = line.src.x;
                b = line.dst.x;
            }

            println!("Drawing horizontal line {:?}", line);
            for x in a ..= b {
                println!("Drawing to {}, {}", x, line.src.y);
                board[x][line.src.y] += 1;
            }
        }else if line.src.x == line.dst.x { // Vertical
            let mut a;
            let mut b;

            // Rust ranges don't operate backwards apparently?
            if line.src.y > line.dst.y {
                a = line.dst.y;
                b = line.src.y;
            }else{
                a = line.src.y;
                b = line.dst.y;
            }

            println!("Drawing vertical line {:?}", line);
            for y in a ..= b {
                println!("Drawing to {}, {}", line.src.x, y);
                board[line.src.x][y] += 1;
            }
        }else{ // Diagonal
            println!("Drawing diagonal line {:?}", line);

            if line.src.x < line.dst.x {
                let mut y = line.src.y;

                if y > line.dst.y {
                    for x in line.src.x ..= line.dst.x {
                        println!("Drawing to {}, {}", x, y);
                        board[x][y] += 1;
                        if y >= 1 { y-=1; }
                    }
                }else{
                    for x in line.src.x ..= line.dst.x {
                        println!("Drawing to {}, {}", x, y);
                        board[x][y] += 1;
                        y+=1;
                    }
                }
            }else{
                let mut y = line.dst.y;

                if y >= line.src.y {
                    for x in line.dst.x ..= line.src.x {
                        println!("Drawing to {}, {}", x, y);
                        board[x][y] += 1;
                        if y >= 1 { y-=1; }
                    }
                }else{
                    for x in line.dst.x ..= line.src.x {
                        println!("Drawing to {}, {}", x, y);
                        board[x][y] += 1;
                        y+=1;
                    }
                }
            }


        }
    }

    print_board(&board);

    let mut overlaps = 0;

    for a in board {
        for b in a {
            if b > 1 {
                overlaps += 1;
            }
        }
    }

    println!("{}", overlaps);
}
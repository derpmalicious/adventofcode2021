use std::fs;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day3p1.txt").unwrap();

    let mut tracker = [0; 12];

    let lines: Vec<&str> = input.lines().collect();
    let lines_len = lines.len();

    for line in lines {
        for i in 0..12 {
            if line.chars().nth(i).unwrap() == '1' {
                tracker[i] += 1;
            }
        }
    }

    let mut gamma = String::new();
    let mut epsilon = String::new();

    for v in tracker {
        if v > (lines_len/2) {
            gamma.push('1');
            epsilon.push('0');
        }else{
            gamma.push('0');
            epsilon.push('1');
        }
    }

    let gamma = i32::from_str_radix(&gamma, 2).unwrap();
    let epsilon = i32::from_str_radix(&epsilon, 2).unwrap();

    println!("{:?}: {}", tracker, gamma * epsilon);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day3p1.txt").unwrap();
    let lines: Vec<&str> = input.lines().collect();
    let mut oxylines = lines.clone();
    let mut co2lines = lines.clone();

    const N: i32 = 12;

    // Oxygen
    for i in 0..N {
        let mut tracker = 0.;

        if oxylines.len() <= 1 {
            break;
        }

        for line in &oxylines {
            if line.chars().nth(i as usize).unwrap() == '1' {
                tracker += 1.;
            }
        }

        if tracker >= oxylines.len() as f32/2. {
            oxylines = oxylines.into_iter().filter(|f| { if f.chars().nth(i as usize).unwrap() == '1' { true } else { false }}).collect();
        }else{
            oxylines = oxylines.into_iter().filter(|f| { if f.chars().nth(i as usize).unwrap() == '0' { true } else { false }}).collect();
        }
    }

    // CO2
    for i in 0..N {
        let mut tracker = 0.;

        if co2lines.len() <= 1 {
            break;
        }

        for line in &co2lines {
            if line.chars().nth(i as usize).unwrap() == '1' {
                tracker += 1.;
            }
        }

        if tracker < co2lines.len() as f32/2. {
            co2lines = co2lines.into_iter().filter(|f| { if f.chars().nth(i as usize).unwrap() == '1' { true } else { false }}).collect();
        }else{
            co2lines = co2lines.into_iter().filter(|f| { if f.chars().nth(i as usize).unwrap() == '0' { true } else { false }}).collect();
        }
    }

    let oxy = i32::from_str_radix(&oxylines[0], 2).unwrap();
    let co2 = i32::from_str_radix(&co2lines[0], 2).unwrap();

    println!("Oxygen: {}, CO2: {}, Answer: {}", oxy, co2, oxy*co2);
}
use std::collections::HashMap;
use std::fs;
use std::str::FromStr;

/*
0 6 abcefg
1 2 cf
2 5 acdeg
3 5 acdfg
4 4 bcdf
5 5 abdfg
6 6 abdefg
7 3 acf
8 7 abcdefg
9 6 abcdfg
 */

fn find_missing_in_b(a: &str, b: &str) -> Vec<String> {
    let mut missing = Vec::new();

    for char in a.chars() {
        if !b.contains(char) {
            missing.push(String::from(char));
        }
    }

    return missing;
}

fn solve(test: &str) -> HashMap<String, String>{
    let mut segments_by_size: HashMap<usize, Vec<&str>> = HashMap::new();
    let mut map = HashMap::new();

    // Put segments into buckets based on size
    for item in test.split(' ') {
        let len = item.len();

        if segments_by_size.contains_key(&len) {
            segments_by_size.get_mut(&len).unwrap().push(item);
        }else{
            segments_by_size.insert(len, vec!(item));
        }
    }

    let mut a = String::new();
    let mut b = String::new();
    let mut c = String::new();
    let mut d = String::new();
    let mut e = String::new();
    let mut f = String::new();
    let mut g = String::new();

    let n1 = segments_by_size.get(&2).unwrap().first().unwrap();
    let n4 = segments_by_size.get(&4).unwrap().first().unwrap();
    let n7 = segments_by_size.get(&3).unwrap().first().unwrap();
    let n8 = segments_by_size.get(&7).unwrap().first().unwrap();

    let mut n0 = "";
    let mut n6 = "";
    let mut n9 = "";

    // 6, 9, 0 : cde
    for n in segments_by_size.get(&6).unwrap() {
        if find_missing_in_b(n1, n).is_empty() { // 9 or 0
            if find_missing_in_b(n4, n).is_empty() { // 9
                n9 = n;
                e = find_missing_in_b(n8, n).first().unwrap().to_string()
            }else{ // 0
                n0 = n;
                d = find_missing_in_b(n4, n).first().unwrap().to_string()
            }
        }else{ // 6
            n6 = n;
            c = find_missing_in_b(n1, n).first().unwrap().to_string();
            f = find_missing_in_b(n1, &c).first().unwrap().to_string();
        }
    }

    let mut n2 = "";
    let mut n3 = "";
    let mut n5 = "";

    // 2, 3, 5
    for n in segments_by_size.get(&5).unwrap() {
        if !n.contains(&c) { // 5
            n5 = n;
            e = find_missing_in_b(n6, n).first().unwrap().to_string();
        }else if !n.contains(&f) { // 2
            n2 = n;
            let mut b_candidates = find_missing_in_b(n8, n);
            b_candidates.retain(|x| x!=&f);
            b = b_candidates.first().unwrap().to_string();
        }else{ // 3

        }
    }

    a = find_missing_in_b(n7, n1).first().unwrap().to_string();
    let mut g_candidates = find_missing_in_b(n9, n4);
    g_candidates.retain(|x| x!=&a);
    g = g_candidates.first().unwrap().to_string();


    map.insert(a, "a".to_string());
    map.insert(b, "b".to_string());
    map.insert(c, "c".to_string());
    map.insert(d, "d".to_string());
    map.insert(e, "e".to_string());
    map.insert(f, "f".to_string());
    map.insert(g, "g".to_string());

    //println!("{:?}", segments_by_size);
    //println!("a: {}, b: {}, c: {}, d: {}, e: {}, f: {}, g: {}", a, b, c, d, e, f, g);
    //println!("{:?}", map);
    map
}

fn find_answer_1(map: HashMap<String, String>, answer: &str) -> [i32; 10]{
    let mut n_map = HashMap::new();

    n_map.insert("abcefg".to_string(), 0);
    n_map.insert("cf".to_string(), 1);
    n_map.insert("acdeg".to_string(), 2);
    n_map.insert("acdfg".to_string(), 3);
    n_map.insert("bcdf".to_string(), 4);
    n_map.insert("abdfg".to_string(), 5);
    n_map.insert("abdefg".to_string(), 6);
    n_map.insert("acf".to_string(), 7);
    n_map.insert("abcdefg".to_string(), 8);
    n_map.insert("abcdfg".to_string(), 9);

    //println!("{}", answer);

    let mut answer_array = [0; 10];

    for word in answer.split(' ') {
        let mut a: Vec<String> = word.chars().map(|x| {
            String::from(map.get(&String::from(x)).unwrap())
        }).collect();
        a.sort();
        let b = a.join("");
        let n = n_map.get(&b.to_string()).unwrap();
        answer_array[*n as usize] += 1;
    }
    answer_array
}

fn find_answer_2(map: HashMap<String, String>, answer: &str) -> i32 {
    let mut n_map = HashMap::new();

    n_map.insert("abcefg".to_string(), 0);
    n_map.insert("cf".to_string(), 1);
    n_map.insert("acdeg".to_string(), 2);
    n_map.insert("acdfg".to_string(), 3);
    n_map.insert("bcdf".to_string(), 4);
    n_map.insert("abdfg".to_string(), 5);
    n_map.insert("abdefg".to_string(), 6);
    n_map.insert("acf".to_string(), 7);
    n_map.insert("abcdefg".to_string(), 8);
    n_map.insert("abcdfg".to_string(), 9);

    //println!("{}", answer);
    let mut a_str = String::new();

    for word in answer.split(' ') {
        let mut a: Vec<String> = word.chars().map(|x| {
            String::from(map.get(&String::from(x)).unwrap())
        }).collect();
        a.sort();
        let b = a.join("");
        let n = n_map.get(&b.to_string()).unwrap();
        a_str.push_str(&n.to_string());
    }

    i32::from_str(&a_str).unwrap()
}

pub fn pt1() {
    let input = fs::read_to_string("inputs/day8p1.txt").unwrap();
    let mut answer_array = [0; 10];

    for line in input.lines() {
        let (test, answer) = line.split_once(" | ").unwrap();
        let map = solve(test);
        let a = find_answer_1(map, answer);
        for i in 0..9 {
            answer_array[i] += a[i];
        }
    }

    // Fuck, I didn't read what was required, part 1 should have been way simpler :D
    println!("{}", answer_array[1] + answer_array[4] + answer_array[7] + answer_array[8]);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day8p1.txt").unwrap();
    let mut ans = 0;

    for line in input.lines() {
        let (test, answer) = line.split_once(" | ").unwrap();
        let map = solve(test);
        let a = find_answer_2(map, answer);
        ans += a;
    }

    // Fuck, I didn't read what was required, part 1 should have been way simpler :D
    println!("{}", ans);
}
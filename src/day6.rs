use std::{fs, usize};
use std::str::FromStr;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day6p1.txt").unwrap();
    let line = input.lines().map(|s| String::from(s) ).collect::<Vec<String>>().pop().unwrap();

    let mut fishes = Vec::new();

    for fish in line.split(',') {
        let counter = i32::from_str(fish).unwrap();
        fishes.push(counter);
    }

    //println!("{:?}", fishes);

    for i in 1..=80 {
        //println!("Day {}", i);
        let mut new_fish = 0;

        for fish in &mut fishes {
            *fish -= 1;

            if *fish < 0 {
                new_fish += 1;
                *fish = 6;
            }
        }

        for _ in 0..new_fish {
            fishes.push(8);
        }

        //println!("{:?}", fishes);
    }

    println!("{}", fishes.len());
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day6p1.txt").unwrap();
    let line = input.lines().map(|s| String::from(s) ).collect::<Vec<String>>().pop().unwrap();

    let mut fishes: Vec<usize> = Vec::new();

    for _ in 0..=8 {
        fishes.push(0);
    }

    for fish in line.split(',') {
        let counter = usize::from_str(fish).unwrap();
        fishes[counter] += 1;
    }

    println!("{:?}", fishes);

    for day in 1..=256 {
        let mut next = 0;

        for i in (0..=8).rev() {
            let tmp;
            tmp = fishes[i];
            fishes[i] = next;
            next = tmp;

            if i == 0 {
                fishes[8] += tmp;
                fishes[6] += tmp;
            }
        }
    }

    let mut total = 0;
    for fish in fishes {
        total += fish;
    }
    println!("{}", total);
}
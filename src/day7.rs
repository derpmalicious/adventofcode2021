use std::fs;
use std::str::FromStr;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day7p1.txt").unwrap();
    let line = input.lines().map(|s| String::from(s) ).collect::<Vec<String>>().pop().unwrap();

    let mut max = 0;
    let mut crabs = Vec::new();

    for num in line.split(',').map(|x| i32::from_str(x).unwrap()) {
        if num > max {
            max = num;
        }

        crabs.push(num);
    }

    let mut cheapest = i32::MAX;

    for pos in 0..=max {
        let mut total_cost = 0;

        for crab in &crabs {
            let cost = (crab - pos).abs();
            total_cost += cost;
        }

        if total_cost < cheapest {
            cheapest = total_cost;
        }

    }

    println!("{}", cheapest);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day7p1.txt").unwrap();
    let line = input.lines().map(|s| String::from(s) ).collect::<Vec<String>>().pop().unwrap();

    let mut max = 0;
    let mut crabs = Vec::new();

    for num in line.split(',').map(|x| i32::from_str(x).unwrap()) {
        if num > max {
            max = num;
        }

        crabs.push(num);
    }

    let mut cheapest = i32::MAX;

    for pos in 0..=max {
        let mut total_cost = 0;

        for crab in &crabs {
            let mut cost = 0;

            // I think there's some simple efficient math thingy for this, but I can't remember and am lazy
            for i in 0 .. (crab - pos).abs() {
                cost += i+1;
            }

            total_cost += cost;
        }

        if total_cost < cheapest {
            cheapest = total_cost;
        }
    }

    println!("{}", cheapest);
}
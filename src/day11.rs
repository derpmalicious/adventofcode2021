use std::fs;
use std::str::FromStr;

pub fn pt1(){
    let input = fs::read_to_string("inputs/day11p1.txt").unwrap();
    let mut table = Vec::new();

    for line in input.lines() {
        let mut line_split: Vec<String> = line.split("").map(|x| String::from(x)).collect();
        line_split.retain(|v| v!="");
        let line_split: Vec<i32> = line_split.iter().map(|x| i32::from_str(x).unwrap()).collect();
        table.push(line_split);
    }

    let mut flashes = 0;

    for _ in 0..100 {
        for y in 0..table.len() {
            for x in 0..table[y].len() {
                table[y][x] += 1;
            }
        }

        for y in 0..table.len() {
            for x in 0..table[y].len() {
                flashes += update(&mut table, x, y, false);
            }
        }
    }

    println!("{:?}", flashes);
}

pub fn pt2(){
    let input = fs::read_to_string("inputs/day11p1.txt").unwrap();
    let mut table = Vec::new();

    for line in input.lines() {
        let mut line_split: Vec<String> = line.split("").map(|x| String::from(x)).collect();
        line_split.retain(|v| v!="");
        let line_split: Vec<i32> = line_split.iter().map(|x| i32::from_str(x).unwrap()).collect();
        table.push(line_split);
    }

    let mut flashes = 0;

    let mut sync_step = 0;

    'l: loop {
        let mut sync = true;
        sync_step += 1;

        for y in 0..table.len() {
            for x in 0..table[y].len() {
                if table[y][x] != 0 {
                    sync = false;
                }

                table[y][x] += 1;
            }
        }

        if sync == true {
            sync_step -= 1;
            break 'l;
        }

        for y in 0..table.len() {
            for x in 0..table[y].len() {
                flashes += update(&mut table, x, y, false);
            }
        }
    }

    println!("{:?}", sync_step);
}

fn update(table: &mut Vec<Vec<i32>>, x: usize, y: usize, inc: bool) -> i32 {
    let mut total = 0;

    if table[y][x] == 0 {
        return 0;
    }

    if inc {
        table[y][x] += 1;
    }

    if table[y][x] > 9 { // Flash
        table[y][x] = 0;

        total +=1;

        if y > 0 { // North
            total += update(table, x, y-1, true)
        }

        if y > 0 && x < table[0].len()-1 { // North east
            total += update(table, x+1, y-1, true)
        }

        if y > 0 && x > 0 { // North west
            total += update(table, x-1, y-1, true)
        }

        if y < table.len()-1 { // South
            total += update(table, x, y+1, true)
        }

        if y < table.len()-1 && x < table[0].len()-1 { // South east
            total += update(table, x+1, y+1, true)
        }

        if y < table.len()-1 && x > 0 { // South West
            total += update(table, x-1, y+1, true)
        }

        if x > 0 { // West
            total += update(table, x-1, y, true)
        }

        if x < table[0].len()-1 { // East
            total += update(table, x+1, y, true)
        }
    }

    return total;
}
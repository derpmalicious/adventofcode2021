use std::fs;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day1p1.txt").unwrap();

    let mut n = 0;
    let mut last = i32::MAX;

    for line in input.lines() {
        let val: i32 = line.parse().unwrap();

        if val > last {
            n += 1;
        }

        last = val;
    }

    println!("{}", n);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day1p1.txt").unwrap();

    let mut n = 0;
    let mut last = i32::MAX;

    for val in input
        .lines()
        .map(|v| -> i32 { v.parse().unwrap() })
        .collect::<Vec<i32>>()
        .windows(3)
        .map(|x| x[0] + x[1] + x[2]) {

        if val > last {
            n += 1;
        }

        last = val;
    }

    println!("{}", n);
}
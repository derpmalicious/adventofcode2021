use std::fs;
use std::vec::IntoIter;

#[derive(Debug)]
struct BoardItem {
    v: i32,
    marked: bool
}

#[derive(Debug)]
struct Board {
    v: Vec<Vec<BoardItem>>
}

fn print_board(board: &Board) {
    for row in &board.v {
        for col in row {
            if col.marked {
                print!("X{:?} ", col.v);
            }else{
                print!("{:?} ", col.v);
            }

        }
        println!();
    }
}

fn pt1_m(mut boards: Vec<Board>, order: Vec<i32>) {

    println!("{:?}", order);

    let mut winner_board_index = None;
    let mut winning_number = None;

    'order: for num in order {
        // Mark
        for board in &mut boards {
            for row in &mut board.v {
                for col in row {
                    if col.v == num {
                        col.marked = true;
                    }
                }
            }
        }

        // Validate
        for (board_index, board) in &mut boards.iter_mut().enumerate() {
            let len = board.v.len();

            // Check columns
            for i in 0..len {
                let mut col_unmarked = false;

                for row in &mut board.v {
                    if row[i].marked == false {
                        col_unmarked = true;
                        break;
                    }
                }

                if !col_unmarked { // All items on this column were marked, wins
                    println!("Board {} wins (cols)", board_index);
                    winner_board_index = Some(board_index);
                    winning_number = Some(num);
                    break 'order;
                }
            }

            // Check rows
            for row in &mut board.v {
                let mut row_unmarked = false;

                for col in row {
                    if col.marked == false {
                        row_unmarked = true;
                        break;
                    }
                }

                if !row_unmarked { // All items on this row were marked, wins
                    println!("Board {} wins (rows)", board_index);
                    winner_board_index = Some(board_index);
                    winning_number = Some(num);
                    break 'order;
                }
            }
        }
    }

    // Print
    let winner_board_index = winner_board_index.unwrap();
    let winner_board = &boards[winner_board_index];
    let winning_number = winning_number.unwrap();

    print_board(&winner_board);

    let mut answer = 0;

    for row in &winner_board.v {
        for col in row {
            if !col.marked {
                answer += col.v;
            }
        }
    }

    answer *= winning_number;

    println!("{}", answer);
}

pub fn pt1() {
    let input = fs::read_to_string("inputs/day4p1.txt").unwrap();

    let lines: Vec<String> = input.lines().map(|x| String::from(x)).collect();
    let mut lines = lines.into_iter();

    let order: Vec<i32> = lines.next().unwrap().split(",").map(|x| x.parse().unwrap()).collect();

    let mut board_id = 0;
    let mut boards: Vec<Board> = vec!();

    // Construct board
    for line in lines {
        if line == "" {
            boards.push(Board{ v: Vec::new() });
            board_id = boards.len() -1;
        }else{
            let mut row = Vec::new();

            for item in line.split(" ") {
                if item.is_empty() {
                    continue;
                }

                let item = BoardItem{v: item.parse().unwrap(), marked: false};
                row.push(item);
            }

            boards[board_id].v.push(row);
        }
    }

    pt1_m(boards, order)
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day4p1.txt").unwrap();

    let lines: Vec<String> = input.lines().map(|x| String::from(x)).collect();
    let mut lines = lines.into_iter();

    let order: Vec<i32> = lines.next().unwrap().split(",").map(|x| x.parse().unwrap()).collect();

    let mut board_id = 0;
    let mut boards: Vec<Board> = vec!();

    // Construct board
    for line in lines {
        if line == "" {
            boards.push(Board{ v: Vec::new() });
            board_id = boards.len() -1;
        }else{
            let mut row = Vec::new();

            for item in line.split(" ") {
                if item.is_empty() {
                    continue;
                }

                let item = BoardItem{v: item.parse().unwrap(), marked: false};
                row.push(item);
            }

            boards[board_id].v.push(row);
        }
    }

    println!("{:?}", order);

    let mut winning_board = None;
    let mut winning_number = None;
    let mut remaining_boards = boards;

    'order: for num in order.clone() {
        if remaining_boards.len() <= 1 {
            winning_board = Some(remaining_boards.pop().unwrap());
            break;
        }

        winning_number = Some(num);

        // Mark
        for board in &mut remaining_boards {
            for row in &mut board.v {
                for col in row {
                    if col.v == num {
                        col.marked = true;
                    }
                }
            }
        }

        boards = remaining_boards;
        remaining_boards = Vec::new();

        // Validate
        'boards: for (board_index, board) in &mut boards.into_iter().enumerate() {
            let len = board.v.len();

            // Check columns
            for i in 0..len {
                let mut col_unmarked = false;

                for row in &board.v {
                    if row[i].marked == false {
                        col_unmarked = true;
                        break;
                    }
                }

                if !col_unmarked { // All items on this column were marked, wins
                    println!("Board {} wins (cols)", board_index);
                    winning_number = Some(num);
                    continue 'boards;
                }
            }

            // Check rows
            for row in &board.v {
                let mut row_unmarked = false;

                for col in row {
                    if col.marked == false {
                        row_unmarked = true;
                        break;
                    }
                }

                if !row_unmarked { // All items on this row were marked, wins
                    println!("Board {} wins (rows)", board_index);
                    winning_number = Some(num);
                    continue 'boards;
                }
            }

            // Neither cols nor rows won, add to remaining boards
            remaining_boards.push(board);
        }

        println!("{}", num);
        for board in &remaining_boards {
            print_board(board);
            println!();
        }
    }

    // Print
    let winner_board = winning_board.unwrap();
    let winning_number = winning_number.unwrap();

    print_board(&winner_board);

    pt1_m(vec!(winner_board), order);
}

// Oof.
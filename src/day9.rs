use std::fs;
use std::str::FromStr;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day9p1.txt").unwrap();
    let mut table = Vec::new();

    for line in input.lines() {
        let mut line_split: Vec<String> = line.split("").map(|x| String::from(x)).collect();
        line_split.retain(|v| v!="");
        let line_split: Vec<i32> = line_split.iter().map(|x| i32::from_str(x).unwrap()).collect();
        table.push(line_split);
    }

    let mut low_points = Vec::new();
    for y in 0..table.len() {
        for x in 0..table[y].len() {
            let mut is_low_point = true;

            if x > 0 && table[y][x-1] <= table[y][x] { is_low_point = false; }
            if x < table[y].len()-1 && table[y][x+1] <= table[y][x] { is_low_point = false; }

            if y > 0 && table[y-1][x] <= table[y][x] { is_low_point = false; }
            if y < table.len()-1 && table[y+1][x] <= table[y][x] { is_low_point = false; }

            if is_low_point {
                low_points.push(table[y][x].clone());
            }
        }
    }

    println!("{:?}", low_points);


    let risk_levels: Vec<i32> = low_points.iter().map(|v| v+1).collect();
    println!("{:?}", risk_levels);

    let score = risk_levels.iter().fold(0, |acc, v| acc + v );

    println!("{:?}", score);
}

fn find_basin_size(table: &mut Vec<Vec<i32>>, x: usize, y: usize) -> i32 {
    let mut total = 0;
    if table[y][x] == -999 || table[y][x] == 9 { // Already visited this or end of basin
        return 0;
    }else{
        table[y][x] = -999;

        total +=1;

        if y > 0 { // North
            total += find_basin_size(table, x, y-1)
        }

        if y < table.len()-1 { // South
            total += find_basin_size(table, x, y+1)
        }

        if x > 0 { // West
            total += find_basin_size(table, x-1, y)
        }

        if x < table[0].len()-1 { // East
            total += find_basin_size(table, x+1, y)
        }
    }

    return total;
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day9p1.txt").unwrap();
    let mut table = Vec::new();

    for line in input.lines() {
        let mut line_split: Vec<String> = line.split("").map(|x| String::from(x)).collect();
        line_split.retain(|v| v!="");
        let line_split: Vec<i32> = line_split.iter().map(|x| i32::from_str(x).unwrap()).collect();
        table.push(line_split);
    }

    let mut basin_table = table.clone();
    let mut basin_sizes = Vec::new();

    for y in 0..table.len() {
        for x in 0..table[y].len() {
            let mut is_low_point = true;

            if x > 0 && table[y][x-1] <= table[y][x] { is_low_point = false; }
            if x < table[y].len()-1 && table[y][x+1] <= table[y][x] { is_low_point = false; }

            if y > 0 && table[y-1][x] <= table[y][x] { is_low_point = false; }
            if y < table.len()-1 && table[y+1][x] <= table[y][x] { is_low_point = false; }

            if is_low_point {
                basin_sizes.push(find_basin_size(&mut basin_table, x, y));
            }
        }
    }

    basin_sizes.sort();
    let (_, basin_sizes) = basin_sizes.split_at(basin_sizes.len()-3);
    let score = basin_sizes.iter().fold(1, |acc, v| acc * v );

    println!("{:?}", basin_sizes);
    println!("{:?}", score);

}
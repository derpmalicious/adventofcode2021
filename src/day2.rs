use std::fs;

pub fn pt1(){
    let input = fs::read_to_string("inputs/day2p1.txt").unwrap();

    let mut x = 0;
    let mut y = 0;

    for line in input.lines() {
        let (command, dist_s) = line.split_once(" ").unwrap();
        let dist: i32 = dist_s.parse().unwrap();

        match command {
            "forward" => {
                x += dist;
            },
            "down" => {
                y += dist;
            },
            "up" => {
                y -= dist;
            },
            _ => {}
        }
    }

    println!("{}, {}: {}", x, y, x*y);
}

pub fn pt2(){
    let input = fs::read_to_string("inputs/day2p1.txt").unwrap();

    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for line in input.lines() {
        let (command, dist_s) = line.split_once(" ").unwrap();
        let dist: i32 = dist_s.parse().unwrap();

        match command {
            "forward" => {
                horizontal += dist;
                depth += aim * dist;
            },
            "down" => {
                aim += dist;
            },
            "up" => {
                aim -= dist;
            },
            _ => {}
        }
    }

    println!("{}, {}: {}", horizontal, depth, horizontal*depth);
}
use std::fs;

pub fn pt1() {
    let input = fs::read_to_string("inputs/day10p1.txt").unwrap();

    let mut invalid_a = 0;
    let mut invalid_square = 0;
    let mut invalid_curly = 0;
    let mut invalid_angled = 0;

    for line in input.lines() {
        let mut expecting = Vec::new();

        for char in line.chars() {
            if char == '(' || char == '[' || char == '{' || char == '<' {
                expecting.push(char);
            }

            if char == ')' || char == ']' || char == '}' || char == '>' {
                let start = expecting.pop().unwrap();
                if (char == ')' && start != '(') || (char == ']' && start != '[') || (char == '}' && start != '{') || (char == '>' && start != '<') {
                    match char {
                        ')' => invalid_a += 1,
                        ']' => invalid_square += 1,
                        '}' => invalid_curly += 1,
                        '>' => invalid_angled += 1,

                        _ => {}
                    }
                    //println!(") {} ] {} }} {} > {}", invalid_a, invalid_square, invalid_curly, invalid_angled);
                    //println!("{}", line);
                    break;
                }
            }
        }
    }

    invalid_a *= 3;
    invalid_square *= 57;
    invalid_curly *= 1197;
    invalid_angled *= 25137;

    println!("{}", invalid_a + invalid_square + invalid_curly + invalid_angled);
}

pub fn pt2() {
    let input = fs::read_to_string("inputs/day10p1.txt").unwrap();

    let mut incomplete_lines = Vec::new();

    for line in input.lines() {
        let mut expecting = Vec::new();
        let mut invalid = false;

        for char in line.chars() {
            if char == '(' || char == '[' || char == '{' || char == '<' {
                expecting.push(char);
            }

            if char == ')' || char == ']' || char == '}' || char == '>' {
                let start = expecting.pop().unwrap();
                if (char == ')' && start != '(') || (char == ']' && start != '[') || (char == '}' && start != '{') || (char == '>' && start != '<') {
                    invalid = true;
                    break;
                }
            }
        }

        if !invalid {
            incomplete_lines.push(line);
        }
    }

    let mut scores = Vec::new();

    for line in incomplete_lines {
        let mut score: u64 = 0;
        let mut expecting = Vec::new();

        for char in line.chars() {
            if char == '(' || char == '[' || char == '{' || char == '<' {
                expecting.push(char);
            }else{
                expecting.pop().unwrap();
            }
        }

        expecting.reverse();

        for char in &expecting {
            score *= 5;

            match char {
                '(' => score += 1,
                '[' => score += 2,
                '{' => score += 3,
                '<' => score += 4,

                _ => {}
            }
        }

        scores.push(score);
    }

    scores.sort();

    println!("{:?}", scores[scores.len()/2]);
}